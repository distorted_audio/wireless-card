/*
 * main.c
 *
 *  Created on: Jan 28, 2019
 *      Author: Luc Chartier
 */

#include "msp430g2553.h"

// PIN OUT
#define PIN_A BIT0      // P2.0
#define PIN_B BIT1      // P2.1
#define PIN_ENABLE BIT2 // P2.2
#define PIN_LED_1 BIT0  // P1.0
#define PIN_LED_2 BIT6  // P1.6
#define PIN_BUTTON BIT3 // P1.3

#define CLOCK_US 2    // Time Between Pulses, 200micro seconds
#define BETWEEN_ZERO 53 // 53 zeros between track1 & 2
#define TRACKS 2        // Number of Tracks

// Timer code from: http://www.ocfreaks.com/msp430-timer-programming-tutorial/
void initTimer_A(void);
void delayMS(int msecs);

void playBit(int sendBit);
void reverseTrack(int track);
void playTrack(int track);
void storeRevTrack(int track);
void writeA(int a);
void writeB(int b);

const char* tracks[] = {
"%B123456781234567^LASTNAME/FIRST^YYMMSSSDDDDDDDDDDDDDDDDDDDDDDDDD?\0", // Track 1
";123456781234567=YYMMSSSDDDDDDDDDDDDDD?\0" // Track 2
};

const int sublen[] = {
  32, 48, 48 };
const int bitlen[] = {
7, 5, 5 };

unsigned int OFCount; // Timer code

char revTrack[41];
unsigned int curTrack = 0;
int dir;

int main(void)
{
   WDTCTL = WDTPW + WDTHOLD;     // Stop watchdog timer

   P1DIR |= (PIN_LED_1 + PIN_LED_2);
   P1DIR &= ~PIN_BUTTON;
   P2DIR |= (PIN_A + PIN_B + PIN_ENABLE);

   P1OUT &= ~PIN_LED_1;
   P1OUT &= ~PIN_LED_2;
   P2OUT &= ~PIN_A;
   P2OUT &= ~PIN_B;
   P2OUT &= ~PIN_ENABLE;

   P1REN |= PIN_BUTTON; // Enable Resistor for SW pin
   P1OUT |= PIN_BUTTON; // Select Pull Up for SW pin

   //Set MCLK = SMCLK = 1MHz
   BCSCTL1 = CALBC1_1MHZ;  //Timer code
   DCOCTL = CALDCO_1MHZ;   //Timer code

   initTimer_A();          //Timer code
   _enable_interrupt();    //Timer code

   for(;;){
//       P1OUT ^= PIN_LED;
//       delayMS(500);
//       if(P1IN & PIN_BUTTON)          // If SW is NOT pressed
//           P1OUT &= ~PIN_LED_1;
//       else{
//           P1OUT |= PIN_LED_1;
//           playTrack(1 + (curTrack++ % 2));
//           delayMS(500);
//       }
       playTrack(1 + (curTrack++ % 2));
       delayMS(50000);
   }
}

// adopted from: https://github.com/samyk/magspoof/blob/master/magspoof.c
// send a single bit out
void playBit(int sendBit)
{
    dir ^=1;
    writeA(dir);
    writeB(!dir);
    delayMS(CLOCK_US);

    if(sendBit){
        dir ^= 1;
        writeA(dir);
        writeB(!dir);
    }

    delayMS(CLOCK_US);
}

// when reversing
void reverseTrack(int track)
{
  int i = 0, j = 0;
  track--; // index 0
  dir = 0;

  while (revTrack[i++] != '\0');
  i--;
  while (i--)
  {
    for (j = bitlen[track]-1; j >= 0; j--)
    {
      playBit((revTrack[i] >> j) & 1);
    }
  }
}

void playTrack(int track)
{
    int i, j, tmp, crc, lrc = 0;
    track--; // index 0
    dir = 0;

    P2OUT |= PIN_ENABLE; // Enable H-Bridge

    // First put out a bunch of leading zeros
    for (i = 0; i < 25; i++) playBit(0);


    for (i = 0; tracks[track][i] != '\0'; i++)
     {
       crc = 1;
       tmp = tracks[track][i] - sublen[track];

       for (j = 0; j < bitlen[track]-1; j++)
       {
         crc ^= tmp & 1;
         lrc ^= (tmp & 1) << j;
         playBit(tmp & 1);
         tmp >>= 1;
       }
       playBit(crc);
   }

    // finish calculating and send last "byte" (LRC)
      tmp = lrc;
      crc = 1;
      for(j = 0; j < bitlen[track]-1; j++)
      {
        crc ^= tmp & 1;
        playBit(tmp & 1);
        tmp >>= 1;
      }
      playBit(crc);

      // if track 1, play 2nd track in reverse (like swiping back?)
       if (track == 0)
       {
         // if track 1, also play track 2 in reverse
         // zeros in between
         for(i = 0; i < BETWEEN_ZERO; i++)
           playBit(0);

         // send second track in reverse
         reverseTrack(2);
       }

       // finish with 0's
       for(i = 0; i < 5 * 5; i++)
         playBit(0);

       P2OUT &= ~PIN_A;
       P2OUT &= ~PIN_B;
       P2OUT &= ~PIN_ENABLE;

}

// stores track for reverse usage later
void storeRevTrack(int track)
{
  int i, j, tmp, crc, lrc = 0;
  track--; // index 0
  dir = 0;

  for (i = 0; tracks[track][i] != '\0'; i++)
  {
    crc = 1;
    tmp = tracks[track][i] - sublen[track];

    for (j = 0; j < bitlen[track]-1; j++)
    {
      crc ^= tmp & 1;
      lrc ^= (tmp & 1) << j;
      tmp & 1 ?
        (revTrack[i] |= 1 << j) :
        (revTrack[i] &= ~(1 << j));
      tmp >>= 1;
    }
    crc ?
      (revTrack[i] |= 1 << 4) :
      (revTrack[i] &= ~(1 << 4));
  }

  // finish calculating and send last "byte" (LRC)
  tmp = lrc;
  crc = 1;
  for (j = 0; j < bitlen[track]-1; j++)
  {
    crc ^= tmp & 1;
    tmp & 1 ?
      (revTrack[i] |= 1 << j) :
      (revTrack[i] &= ~(1 << j));
    tmp >>= 1;
  }
  crc ?
    (revTrack[i] |= 1 << 4) :
    (revTrack[i] &= ~(1 << 4));

  i++;
  revTrack[i] = '\0';
}

void writeA(int a)
{
    if(a) P2OUT |= PIN_A;
    else P2OUT &= ~PIN_A;
}

void writeB(int b)
{
    if(b) P2OUT |= PIN_B;
    else P2OUT &= ~PIN_B;
}

// Timer code
void initTimer_A(void)
{
    //Timer0_A3 Configuration
    TACCR0 = 0; //Initially, Stop the Timer
    TACCTL0 |= CCIE; //Enable interrupt for CCR0.
    TACTL = TASSEL_2 + ID_0 + MC_1; //Select SMCLK, SMCLK/1, Up Mode
}


// Timer code
void delayMS(int msecs)
{
    P1OUT |= PIN_LED_2;
    OFCount = 0; //Reset Over-Flow counter
    TACCR0 = 100-1; //Start Timer, Compare value for Up Mode to get 1ms delay per loop
    //Total count = TACCR0 + 1. Hence we need to subtract 1.

    while(OFCount<=msecs);

    TACCR0 = 0; //Stop Timer
    P1OUT &= ~PIN_LED_2;
}

// Timer code
//Timer ISR
#pragma vector = TIMER0_A0_VECTOR
__interrupt void Timer_A_CCR0_ISR(void)
{
    OFCount++; //Increment Over-Flow Counter
}
